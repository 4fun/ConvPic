package com.weichal.app;

import com.weichal.utils.ImageUtil;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

/**
 * ConvPicLauncher
 *
 * @author Fred on 2019/11/2.
 * @version 1.0
 */
public class ConvPicLauncher extends Application {
  // 主窗口
  private Stage stage;
  // 左侧图片展示区
  private ImageView leftImg;
  // 右侧图片展示区
  private ImageView rightImg;
  // 上次打开目录
  private File lastPath;
  // 本次选取的图片文件
  private File image;
  // 图片处理器
  private ImageUtil imageUtil;

  @Override
  public void start(Stage primaryStage) throws Exception {
    stage = primaryStage;
    // 总布局为上下结构，使用 VBox 控件
    VBox mainView = new VBox();
    var container = mainView.getChildren();
    // 添加操作控件
    var top = addTop();
    container.add(top);
    // 添加分隔符
    Separator hSep = new Separator(Orientation.HORIZONTAL);
    container.add(hSep);
    // 下部分为展示区
    var bottom = addBottom();
    container.add(bottom);
    VBox.setVgrow(bottom, Priority.ALWAYS);

    Scene scene = new Scene(mainView);
    scene.getStylesheets().add("file:./resources/styles/base.css");
    stage.setScene(scene);
    stage.setMinWidth(220);
    stage.setWidth(800);
    stage.setHeight(600);
    stage.setResizable(false);
    stage.setTitle("图片格式转换工具 V1.0");
    stage.show();
  }

  /**
   * 添加顶部操作控件
   *
   * @return 返回包含操作控件的顶部容器
   */
  private TilePane addTop() {
    TilePane top = new TilePane();

    Button addImg = new Button("选择图片");
    Button beginConv = new Button("开始转换");

    top.getChildren().addAll(addImg, beginConv);
    top.setPadding(new Insets(6, 12, 6, 12));
    top.setHgap(50);
    top.setAlignment(Pos.CENTER);
    // 添加图片事件处理
    addImg.setOnAction(
        event -> {
          // 获取选择的图片
          FileChooser fileChooser = new FileChooser();
          if (lastPath != null) {
            fileChooser.setInitialDirectory(lastPath);
          }
          File file = fileChooser.showOpenDialog(stage);
          this.image = file;
          if (file != null) {
            // 记录上次打开目录
            lastPath = file.getParentFile();
            // 更换图片
            try {
              // 获取图片路径
              String url = file.getAbsoluteFile().toURI().toURL().toString();
              Image img = new Image(url);
              // 首先清理图片 fitWith 和 fitHeight 属性
              // 解绑
              leftImg.fitHeightProperty().unbind();
              leftImg.fitWidthProperty().unbind();
              leftImg.setFitWidth(0);
              leftImg.setFitHeight(0);
              // 添加图片
              leftImg.setImage(img);
              // 得到图片尺寸
              imageUtil = new ImageUtil(this.image);
              double imgWidth = this.imageUtil.getImgWith();
              double imgHeight = this.imageUtil.getImgHeight();
              // 设置图片显示尺寸
              if (imgWidth > 400 || imgHeight > 400) {
                if (imgWidth > imgHeight) {
                  leftImg.fitWidthProperty().bind(((HBox) leftImg.getParent()).widthProperty());
                } else {
                  leftImg.fitHeightProperty().bind(((HBox) leftImg.getParent()).heightProperty());
                }
              }
            } catch (MalformedURLException e) {
              e.printStackTrace();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
        });

    // 添加转换事件
    beginConv.setOnAction(event -> {
      try {
        if (imageUtil.PngToJpg()) {
            showAlert("转换成功！");
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
      // 清除状态

      // 显示图片

      // 保存图片
    });
    return top;
  }

  private HBox addBottom() {
    HBox bottom = new HBox();
    // 添加图片
    HBox left = new HBox();
    left.setMaxWidth(400);
    left.setMinWidth(400);
    HBox right = new HBox();
    ImageView imgLeft = new ImageView();
    leftImg = imgLeft;
    imgLeft.getStyleClass().add("img-left");
    leftImg.setPreserveRatio(true);
    ImageView imgRight = new ImageView();
    rightImg = imgRight;
    imgRight.getStyleClass().add("img-right");
    rightImg.setPreserveRatio(true);
    left.getChildren().add(imgLeft);
    right.getChildren().add(imgRight);
    // 居中显示
    left.setAlignment(Pos.CENTER);
    right.setAlignment(Pos.CENTER);
    // 设置自动拉伸
    Separator vSep = new Separator();
    vSep.setOrientation(Orientation.VERTICAL);
    bottom.getChildren().addAll(left, vSep, right);
    HBox.setHgrow(left, Priority.ALWAYS);
    HBox.setHgrow(right, Priority.ALWAYS);
    return bottom;
  }

  public void showAlert(String msg) {
    showAlert(msg, Alert.AlertType.INFORMATION);
  }

  private void showAlert(String msg, Alert.AlertType type) {
    Alert alert = new Alert(type);
    alert.setContentText(msg);
    alert.show();
  }
}
