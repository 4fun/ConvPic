package com.weichal.app;

import javafx.application.Application;

import static javafx.application.Application.launch;

/**
 * ConvPicApp
 *
 * @author Fred on 2019/11/2.
 * @version 1.0
 */
public class ConvPicApp {
    public static void main(String[] args) {
        launch(ConvPicLauncher.class, args);
    }
}
