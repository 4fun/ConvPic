package com.weichal.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * ImageUtil
 *
 * @author Fred on 2019/11/6.
 * @version 1.0
 */
public class ImageUtil {
  // 图片文件
  private File imgFile;
  // 读取的图片文件
  private BufferedImage image;

  public ImageUtil(File image) throws IOException {
    imgFile = image;
    this.image = ImageIO.read(image);
  }

  public void setImage(File image) throws IOException {
    imgFile = image;
    this.image = ImageIO.read(image);
  }

  public boolean PngToJpg() throws IOException {
    BufferedImage newImg = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
    newImg.createGraphics().drawImage(image, 0, 0, Color.white, null);
    ImageIO.write(newImg, "jpg", new File("D://test.jpg"));
    return true;
  }
  /**
   * 获取图片宽度
   *
   * @return
   */
  public double getImgWith() {
    return image.getWidth();
  }

  /**
   * 获取图片高度
   * @return
   */
  public double getImgHeight() {
    return image.getHeight();
  }
}
